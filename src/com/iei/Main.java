package com.iei;

import com.iei.hospital.Hospital;
import com.iei.hospital.building.Building;
import com.iei.hospital.building.Floor;
import com.iei.hospital.disease.Disease;
import com.iei.hospital.disease.DiseaseType;
import com.iei.hospital.room.*;
import com.iei.hospital.service.Service;
import com.iei.hospital.employee.healthcare.ChiefOfService;
import com.iei.hospital.employee.Director;
import com.iei.hospital.patient.Patient;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("## SETTING UP THE HOSPITAL ##");
        Director director = Director.getInstance();
        director.setName("Jean Pierre");

        Building building = new Building("Franco-Vietnamese Hospital Building", "1000 Hoang Van Thu St., District 1, HCMC");
        Floor floor1 = director.addNewFloor(building, "Floor 1");
        Floor floor2 = director.addNewFloor(building, "Floor 2");
        Floor floor3 = director.addNewFloor(building, "Floor 3");

        Hospital hospital = new Hospital("Franco-Vietnamese Hospital", building);
        hospital.setDirector(director);
        director.hireHumanResourceDirector("John Smith");

        System.out.println("");
        System.out.println("## SETTING UP ROOMS ##");
        director.addRoomToFloor(floor1, RoomType.OFFICE_ROOM, "Office Room - Floor 1", 3);
        director.addRoomToFloor(floor1, RoomType.PATIENT_ROOM, "Single Patient Room 1", 1);
        director.addRoomToFloor(floor1, RoomType.PATIENT_ROOM, "Single Patient Room 2", 1);
        director.addRoomToFloor(floor1, RoomType.PATIENT_ROOM, "Single Patient Room 3", 1);
        StorageRoom storageRoom1 = (StorageRoom) director.addRoomToFloor(floor1, RoomType.STORAGE_ROOM, "Storage Room 1", 0);
        StorageRoom storageRoom2 = (StorageRoom) director.addRoomToFloor(floor1, RoomType.STORAGE_ROOM, "Storage Room 2", 0);
        director.addRoomToFloor(floor2, RoomType.PATIENT_ROOM, "Single Patient Room 4", 1);
        director.addRoomToFloor(floor2, RoomType.PATIENT_ROOM, "Two-Patient Room 1", 2);
        director.addRoomToFloor(floor2, RoomType.PATIENT_ROOM, "Two-Patient Room 2", 2);
        director.addRoomToFloor(floor2, RoomType.PATIENT_ROOM, "Two-Patient Room 3", 2);
        StorageRoom storageRoom3 = (StorageRoom) director.addRoomToFloor(floor1, RoomType.STORAGE_ROOM, "Storage Room 2", 0);
        director.addRoomToFloor(floor3, RoomType.OPERATION_ROOM, "Operation Room", 20);
        OfficeRoomInterface directorRoom = new DirectorRoomProxy((OfficeRoomInterface) director.addRoomToFloor(floor3, RoomType.OFFICE_ROOM, "Director Room", 1));
        directorRoom.addEmployee(director);

        // setup services
        System.out.println("");
        System.out.println("## ADDING SERVICES ##");
        Service pediatricService = director.addNewService(hospital, floor1, "Pediatric Service");
        pediatricService.setStorageRoom(storageRoom1);
        Service geriatricService = director.addNewService(hospital, floor1, "Geriatric Service");
        geriatricService.setStorageRoom(storageRoom2);
        Service emergencyService = director.addNewService(hospital, floor2, "Emergency Service");
        emergencyService.setStorageRoom(storageRoom3);

        ChiefOfService pediatricServiceChief = new ChiefOfService("John Peter");
        pediatricService.setChiefOfService(pediatricServiceChief);

        ChiefOfService emergencyServiceChief = new ChiefOfService("John Wick");
        emergencyService.setChiefOfService(emergencyServiceChief);

        // patients come to the hospital
        System.out.println("");
        System.out.println("## DEMO ##");
        System.out.println("## PATIENTS GO TO HOSPITAL ###");
        Patient patient1 = new Patient("Patient 1", new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE));
        emergencyService.patientRegister(patient1);
        Patient patient2 = new Patient("Patient 2", new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE));
        emergencyService.patientRegister(patient2);
        Patient patient3 = new Patient("Patient 3", new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE));
        emergencyService.patientRegister(patient3);
        Patient patient4 = new Patient("Patient 4", new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE));
        pediatricService.patientRegister(patient4);
        Patient patient5 = new Patient("Patient 5", new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE));
        pediatricService.patientRegister(patient5);

        Patient patient6 = new Patient("Patient 6", new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE));
        emergencyService.patientRegister(patient6);
        Patient patient7 = new Patient("Patient 7", new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE));
        emergencyService.patientRegister(patient7);
        Patient patient8 = new Patient("Patient 8", new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE));
        emergencyService.patientRegister(patient8);
        Patient patient9 = new Patient("Patient 9", new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE));
        emergencyService.patientRegister(patient9);
        Patient patient10 = new Patient("Patient 10", new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE));
        pediatricService.patientRegister(patient10);

        for (int i = 1; i <= 5; i++) {
            System.out.println("");
            System.out.println("## EMPLOYEES ARE GOING TO WORK - ACTIVITY " + i + " ##");
            hospital.checkPatients();
            System.out.println("");
            System.out.println("## EXITING HEALED PATIENTS (IF ANY) - ACTIVITY " + i + " ##");
            hospital.checkOutPatients();
        }

        startInteractiveMode(hospital, emergencyService);
    }

    /**
     * Start interactive mode
     * @param hospital
     * @param service
     */
    public static void startInteractiveMode(Hospital hospital, Service service) {
        System.out.println("");
        System.out.println("## INTERACTIVE MODE ##");

        Scanner reader = new Scanner(System.in).useDelimiter("\n");

        boolean keepInteractiveMode = true;
        int numberOfPatients = 0;

        do {
            System.out.print("Enter number of patients, or enter 0 to exit: ");

            try {
                numberOfPatients = reader.nextInt();
            } catch (Exception exception) {}

            if (numberOfPatients > 0) {
                for (int i = 1; i <= numberOfPatients; i++) {
                    System.out.println("Enter information for patient " + i);
                    System.out.print("Enter name: ");
                    String name = reader.next();
                    System.out.print("Enter disease type: 1. Medical Type; 2. Operation Type: ");
                    int diseaseType = reader.nextInt();

                    Disease disease = (diseaseType == 1)
                            ? new Disease("MedicalDisease 1", DiseaseType.MEDICAL_TYPE)
                            : new Disease("MedicalDisease 2", DiseaseType.OPERATION_TYPE);
                    Patient patient = new Patient(name, disease);
                    service.patientRegister(patient);
                }

                for (int i = 1; i <= 5; i++) {
                    System.out.println("");
                    System.out.println("## EMPLOYEES ARE GOING TO WORK - ACTIVITY " + i + " ##");
                    hospital.checkPatients();
                    System.out.println("");
                    System.out.println("## EXITING HEALED PATIENTS (IF ANY) - ACTIVITY " + i + " ##");
                    hospital.checkOutPatients();

                    System.out.println("");
                }
            } else {
                keepInteractiveMode = false;
            }
        } while (keepInteractiveMode);

        reader.close();
    }
}
