package com.iei.hospital.service;

import com.iei.hospital.patient.Patient;

/**
 * Service observer
 */
public interface ServiceObserver {

    /**
     * Listen on patient registration event
     * @param service
     * @param patient
     */
    void onPatientRegistration(Service service, Patient patient);

    /**
     * Listen on patient check-in event
     * @param service
     * @param patient
     */
    void onPatientCheckIn(Service service, Patient patient);

}
