package com.iei.hospital.service;

import com.iei.hospital.Hospital;
import com.iei.hospital.building.Floor;
import com.iei.hospital.employee.healthcare.ChiefOfService;
import com.iei.hospital.employee.healthcare.Doctor;
import com.iei.hospital.employee.healthcare.Nurse;
import com.iei.hospital.employee.humanresource.HumanResourceEmployee;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.room.StorageRoom;

import java.util.*;

/**
 * Service entity
 */
public class Service {

    /**
     * Service name
     */
    private final String name;

    /**
     * Floor which service belongs to
     */
    private final Floor floor;

    /**
     * Hospital
     */
    private Hospital hospital;

    /**
     * Storage room
     */
    private StorageRoom storageRoom;

    /**
     * Chief of service
     */
    private ChiefOfService chiefOfService;

    /**
     * Human Resource for employees in this service
     */
    private HumanResourceEmployee employeeHumanResource;

    /**
     * Human Resource for patients in this service
     */
    private HumanResourceEmployee patientHumanResource;

    /**
     * List of doctors in this service
     */
    private final List<Doctor> doctors = new LinkedList<>();

    /**
     * List of nurse in this service
     */
    private final List<Nurse> nurses = new LinkedList<>();

    /**
     * Patients got healed and waiting for checkout
     */
    private final List<Patient> patientsHealed = new LinkedList<>();

    /**
     * Patients in registration queue
     */
    private final Queue<Patient> patientsRegistrationQueue = new LinkedList<>();

    /**
     * Patients in service
     */
    private final List<Patient> patientsInService = new LinkedList<>();

    /**
     * Service observers
     */
    private final List<ServiceObserver> serviceObservers = new LinkedList<>();

    /**
     * Constructor
     * @param hospital
     * @param floor
     * @param name
     */
    public Service(Hospital hospital, Floor floor, String name) {
        this.name = name;
        this.floor = floor;
        this.hospital = hospital;
        this.floor.addService(this);
    }

    /**
     * Patient Registration
     * @param patient
     */
    public void patientRegister(Patient patient) {
        this.patientsRegistrationQueue.add(patient);
        System.out.println("");
        System.out.println("["+ patient +"] has come to " + this);

        this.serviceObservers.forEach(e -> e.onPatientRegistration(this, patient));
    }

    /**
     * Accept patient
     * @param patient
     * @param doctor
     * @param nurse
     */
    public void acceptPatient(Patient patient, Doctor doctor, Nurse nurse) {
        doctor.addPatient(patient);
        nurse.addPatient(patient);
        this.patientsInService.add(patient);

        this.serviceObservers.forEach(e -> e.onPatientCheckIn(this, patient));
    }

    /**
     * Add observer
     * @param serviceObserver
     */
    public void addObserver(ServiceObserver serviceObserver) {
        this.serviceObservers.add(serviceObserver);
    }

    /**
     * Remove observer
     * @param serviceObserver
     */
    public void removeObserver(ServiceObserver serviceObserver) {
        this.serviceObservers.remove(serviceObserver);
    }

    /**
     * Check if there are any patients in registration queue
     * @return boolean
     */
    public boolean hasPatientsInRegistrationQueue() {
        return !this.patientsRegistrationQueue.isEmpty();
    }

    /**
     * Get a patient in registration queue
     * @return
     */
    public Patient getPatientInRegistrationQueue() {
        return this.patientsRegistrationQueue.remove();
    }

    /**
     * Get list of patients in service
     * @return
     */
    public List<Patient> getPatientsInService() {
        return patientsInService;
    }

    /**
     * Set patient as healed
     * @param patient
     */
    public void patientHealed(Patient patient) {
        this.patientsHealed.add(patient);
        this.patientsInService.remove(patient);
    }

    /**
     * Add doctor to this service
     * @param doctor
     */
    public void addDoctor(Doctor doctor) {
        this.doctors.add(doctor);
        doctor.setService(this);
    }

    /**
     * Get available doctor
     * @return
     */
    public Optional<Doctor> getAvailableDoctor() {
        return this.doctors.stream().min(Comparator.comparing(Doctor::getNumberOfPatients));
    }

    /**
     * Get list of service
     * @return
     */
    public List<Doctor> getDoctors() {
        return doctors;
    }

    /**
     * Add nurse to the service
     * @param nurse
     */
    public void addNurse(Nurse nurse) {
        this.nurses.add(nurse);
        nurse.setService(this);
    }

    /**
     * Get available nurse
     * @return
     */
    public Optional<Nurse> getAvailableNurse() {
        return this.nurses.stream().min(Comparator.comparing(Nurse::getNumberOfPatients));
    }

    /**
     * Get list of nurse in the service
     * @return
     */
    public List<Nurse> getNurses() {
        return nurses;
    }

    /**
     * Get chief of service
     * @return
     */
    public ChiefOfService getChiefOfService() {
        return chiefOfService;
    }

    /**
     * Set chief of service
     * @param chiefOfService
     */
    public void setChiefOfService(ChiefOfService chiefOfService) {
        this.chiefOfService = chiefOfService;
        this.chiefOfService.setService(this);
    }

    /**
     * Get service name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get floor
     * @return
     */
    public Floor getFloor() {
        return floor;
    }

    /**
     * Set hospital
     * @param hospital
     */
    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    /**
     * Get hospital
     * @return
     */
    public Hospital getHospital() {
        return hospital;
    }

    /**
     * Set storage room for the service
     * @param storageRoom
     */
    public void setStorageRoom(StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
        this.storageRoom.setService(this);
        System.out.println(this + " now has " + storageRoom);
    }

    /**
     * Get storage room
     * @return
     */
    public StorageRoom getStorageRoom() {
        return storageRoom;
    }

    /**
     * Get list of healed patients
     * @return
     */
    public List<Patient> getPatientsHealed() {
        return patientsHealed;
    }

    /**
     * Patient checkout
     * @param patient
     */
    public void checkoutPatient(Patient patient) {
        this.patientsHealed.remove(patient);
        patient.getPatientRoom().removePatient(patient);
        System.out.println(patient + " has been checked out from " + this.hospital);
    }

    /**
     * Set human resource for employees
     * @param employeeHumanResource
     */
    public void setEmployeeHumanResource(HumanResourceEmployee employeeHumanResource) {
        this.employeeHumanResource = employeeHumanResource;
    }

    /**
     * Set human resource for patients
     * @param patientHumanResource
     */
    public void setPatientHumanResource(HumanResourceEmployee patientHumanResource) {
        this.patientHumanResource = patientHumanResource;
    }

    /**
     * Get human resource for employees
     * @return
     */
    public HumanResourceEmployee getEmployeeHumanResource() {
        return employeeHumanResource;
    }

    /**
     * Get human resource for patients
     * @return
     */
    public HumanResourceEmployee getPatientHumanResource() {
        return patientHumanResource;
    }

    @Override
    public String toString() {
        return "Service{" +
                "name='" + name + '\'' +
                '}';
    }

}

