package com.iei.hospital.service;

import com.iei.hospital.patient.Patient;

/**
 * Observer that listens on service's activities
 */
public class ServiceActivityObserver implements ServiceObserver {

    /**
     * Listen on patient registration event
     * @param service
     * @param patient
     */
    @Override
    public void onPatientRegistration(Service service, Patient patient) {
        System.out.println("[ServiceActivityObserver] There is a new patient has come to " + service
                + ". Notifying " + service.getChiefOfService());
        service.getChiefOfService().checkPatientsInQueue();
    }

    /**
     * Listen on patient check-in event
     * @param service
     * @param patient
     */
    @Override
    public void onPatientCheckIn(Service service, Patient patient) {
        System.out.println("[ServiceActivityObserver] " + patient + " has been added to " + service);
        service.getPatientHumanResource().acknowledgePatientArrival(service, patient);
    }

}
