package com.iei.hospital.building;

import com.iei.hospital.room.Room;
import com.iei.hospital.service.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Floor entity
 */
public class Floor {

    /**
     * Name of floor
     */
    private String name;

    /**
     * List of rooms on this floor
     */
    private final List<Room> rooms = new LinkedList<>();

    /**
     * List of services on this floor
     */
    private final List<Service> services = new LinkedList<>();

    /**
     * Constructor
     * @param name
     */
    public Floor(String name) {
        this.name = name;
    }

    /**
     * Get name of the floor
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of the floor
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Add room to the floor
     * @param room
     */
    public void addRoom(Room room) {
        this.rooms.add(room);
    }

    /**
     * Add service to the floor
     * @param service
     */
    public void addService(Service service) {
        this.services.add(service);
    }

    /**
     * Get list of rooms on this floor
     * @return
     */
    public List<Room> getRooms() {
        return rooms;
    }

    /**
     * Get list of services on this floor
     * @return
     */
    public List<Service> getServices() {
        return services;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "name='" + name + '\'' +
                '}';
    }
}
