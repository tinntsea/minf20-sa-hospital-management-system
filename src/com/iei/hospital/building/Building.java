package com.iei.hospital.building;

import java.util.HashSet;
import java.util.Set;

public class Building {

    /**
     * Building name
     */
    private final String name;

    /**
     * Building address
     */
    private final String address;

    /**
     * Floors of the hospital
     */
    private final Set<Floor> floors = new HashSet<>();

    /**
     * Constructor
     * @param name
     * @param address
     */
    public Building(String name, String address) {
        this.name = name;
        this.address = address;
    }

    /**
     * Add a new floor to the hospital
     * @param floor
     */
    public void addFloor(Floor floor) {
        this.floors.add(floor);
    }

    /**
     * Get list of floors of the hospital
     * @return
     */
    public Set<Floor> getFloors() {
        return floors;
    }

}
