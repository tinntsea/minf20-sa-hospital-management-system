package com.iei.hospital.equipment;

/**
 * Equipment enum
 */
public enum EquipmentType {
    HEALTHCARE_EQUIPMENT, STAFF_EQUIPMENT
}
