package com.iei.hospital.equipment;

/**
 * Equipment entity
 */
public class Equipment {

    /**
     * Equipment type
     */
    private final EquipmentType type;

    /**
     * Equipment name
     */
    private final String name;

    /**
     * Constructor
     * @param type
     * @param name
     */
    public Equipment(EquipmentType type, String name) {
        this.type = type;
        this.name = name;
    }

    /**
     * Get equipment type
     * @return
     */
    public EquipmentType getType() {
        return type;
    }

    /**
     * Get equipment name
     * @return
     */
    public String getName() {
        return name;
    }
}
