package com.iei.hospital.patient;

import com.iei.hospital.disease.DiseaseType;
import com.iei.hospital.employee.healthcare.Doctor;
import com.iei.hospital.equipment.Equipment;
import com.iei.hospital.room.Room;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Treatment Activity Record
 */
public class TreatmentActivityRecord {

    /**
     * Doctor who did this activity
     */
    private final Doctor doctor;

    /**
     * Patient
     */
    private final Patient patient;

    /**
     * Disease Type
     */
    private final DiseaseType diseaseType;

    /**
     * Room
     */
    private final Room room;

    /**
     * Equipment consumed
     */
    private final List<Equipment> equipmentsConsumed;

    /**
     * Datetime
     */
    private final LocalDateTime dateTime;

    /**
     * Constructor
     * @param doctor
     * @param patient
     * @param diseaseType
     * @param room
     * @param equipmentsConsumed
     */
    public TreatmentActivityRecord(Doctor doctor, Patient patient, DiseaseType diseaseType, Room room, List<Equipment> equipmentsConsumed) {
        this.doctor = doctor;
        this.patient = patient;
        this.diseaseType = diseaseType;
        this.room = room;
        this.equipmentsConsumed = equipmentsConsumed;
        this.dateTime = LocalDateTime.now();
    }

    /**
     * Get doctor
     * @return
     */
    public Doctor getDoctor() {
        return doctor;
    }

    /**
     * Get patient
     * @return
     */
    public Patient getPatient() {
        return patient;
    }

    /**
     * Get disease type
     * @return
     */
    public DiseaseType getDiseaseType() {
        return diseaseType;
    }

    /**
     * Get room
     * @return
     */
    public Room getRoom() {
        return room;
    }

    /**
     * Get equipments consumed
     * @return
     */
    public List<Equipment> getEquipmentsConsumed() {
        return equipmentsConsumed;
    }

    /**
     * Get datetime
     * @return
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
