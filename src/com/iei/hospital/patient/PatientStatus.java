package com.iei.hospital.patient;

/**
 * Patient status enum
 */
public enum PatientStatus {
    SICK, HEALED
}
