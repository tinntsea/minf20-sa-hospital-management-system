package com.iei.hospital.patient;

import com.iei.hospital.disease.Disease;
import com.iei.hospital.room.PatientRoom;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

/**
 * Patient entity
 */
public class Patient {

    /**
     * Patient name
     */
    private final String name;

    /**
     * Patient's disease
     */
    private final Disease disease;

    /**
     * Patient Status
     */
    private PatientStatus status;

    /**
     * Patient's room
     */
    private PatientRoom patientRoom;

    /**
     * Checked in date
     */
    private final LocalDateTime checkedInDate;

    /**
     * Checked out date
     */
    private LocalDateTime checkedOutDate;

    /**
     * Treatment activity records
     */
    private final List<TreatmentActivityRecord> treatmentActivityRecords = new LinkedList<>();

    /**
     * Constructor
     * @param name
     * @param disease
     */
    public Patient(String name, Disease disease) {
        this.name = name;
        this.disease = disease;
        this.status = PatientStatus.SICK;
        this.checkedInDate = LocalDateTime.now();
    }

    /**
     * Get patient's name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get patient's disease
     * @return
     */
    public Disease getDisease() {
        return disease;
    }

    /**
     * Get patient's status
     * @return
     */
    public PatientStatus getStatus() {
        return status;
    }

    /**
     * Set patient's status
     * @param status
     */
    public void setStatus(PatientStatus status) {
        this.status = status;
    }

    /**
     * Get patient's room
     * @return
     */
    public PatientRoom getPatientRoom() {
        return patientRoom;
    }

    /**
     * Get patient's room
     * @param patientRoom
     */
    public void setPatientRoom(PatientRoom patientRoom) {
        this.patientRoom = patientRoom;
    }

    /**
     * Set checked out date
     * @param checkedOutDate
     */
    public void setCheckedOutDate(LocalDateTime checkedOutDate) {
        this.checkedOutDate = checkedOutDate;
    }

    /**
     * Get checked in date
     * @return
     */
    public LocalDateTime getCheckedInDate() {
        return checkedInDate;
    }

    /**
     * Get checked out date
     * @return
     */
    public LocalDateTime getCheckedOutDate() {
        return checkedOutDate;
    }

    /**
     * Add treatment activity record
     * @param treatmentActivityRecord
     */
    public void addTreatmentActivityRecord(TreatmentActivityRecord treatmentActivityRecord) {
        this.treatmentActivityRecords.add(treatmentActivityRecord);
    }

    /**
     * Get list of treatment activity records
     * @return
     */
    public List<TreatmentActivityRecord> getTreatmentActivityRecords() {
        return treatmentActivityRecords;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "name='" + name + '\'' +
                '}';
    }
}
