package com.iei.hospital.employee;

public interface Employee {

    /**
     * Get employee name
     * @return String
     */
    String getName();

    /**
     * Get employee position
     * @return String
     */
    String getPosition();

    /**
     * Get license of the employee
     * @return String
     */
    String getLicense();

}
