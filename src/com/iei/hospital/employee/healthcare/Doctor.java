package com.iei.hospital.employee.healthcare;

import com.iei.hospital.disease.DiseaseType;
import com.iei.hospital.equipment.Equipment;
import com.iei.hospital.equipment.EquipmentType;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.patient.PatientStatus;
import com.iei.hospital.patient.TreatmentActivityRecord;
import com.iei.hospital.room.Room;
import com.iei.hospital.service.Service;
import com.iei.util.NumberUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Doctor entity
 */
public class Doctor extends AbstractHealthCareEmployee {

    /**
     * Constructor
     * @param name
     */
    public Doctor(String name) {
        super(name);
    }

    public void checkPatients() {
        System.out.println(this + " has started working");
        ArrayList<Patient> patients =  new ArrayList<>(this.getPatients());
        for (Patient patient : patients) {
            System.out.println(this + " is trying to cure " + patient);

            int probability = NumberUtil.getRandomInt(1, 100);
            List<Equipment> equipments = new LinkedList<>();
            Room room = null;
            switch (patient.getDisease().getType()) {
                case MEDICAL_TYPE:
                    equipments = this.service.getHospital().getEquipments(EquipmentType.HEALTHCARE_EQUIPMENT, 5);

                    if (probability <= 80) {
                        patient.setStatus(PatientStatus.HEALED);

                        System.out.println(patient + " has been cured");
                    } else if (probability <= 95) {
                        System.out.println(patient + " situation stays the same");
                    } else {
                        patient.getDisease().setType(DiseaseType.OPERATION_TYPE);

                        System.out.println(patient + " situation changed to " + DiseaseType.OPERATION_TYPE);
                    }

                    room = patient.getPatientRoom();
                    break;
                case OPERATION_TYPE:
                    equipments = this.service.getHospital().getEquipments(EquipmentType.HEALTHCARE_EQUIPMENT, 10);

                    if (probability <= 70) {
                        patient.setStatus(PatientStatus.HEALED);
                        System.out.println(patient + " has been cured");
                    } else if (probability <= 85) {
                        System.out.println(patient + " situation stays the same");
                    } else if (probability <= 95) {
                        patient.setStatus(PatientStatus.HEALED);
                        System.out.println(patient + " has been cured");
                    } else {
                        patient.getDisease().setType(DiseaseType.MEDICAL_TYPE);
                        System.out.println(patient + " situation changed to " + DiseaseType.MEDICAL_TYPE);
                    }

                    room = this.service.getHospital().getOperationRoom();
                    break;
            }

            if (patient.getStatus() == PatientStatus.HEALED) {
                service.patientHealed(patient);
                this.removePatient(patient);
            }

            TreatmentActivityRecord treatmentActivityRecord = new TreatmentActivityRecord(this, patient, patient.getDisease().getType(), room, equipments);
            patient.addTreatmentActivityRecord(treatmentActivityRecord);
        }
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "name='" + name + '\'' +
                '}';
    }
}
