package com.iei.hospital.employee.healthcare;

import com.iei.hospital.Hospital;
import com.iei.hospital.building.Floor;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.room.PatientRoom;
import com.iei.hospital.room.Room;
import com.iei.hospital.service.Service;

/**
 * Chief of service entity
 */
public class ChiefOfService extends AbstractHealthCareEmployee {


    /**
     * Constructor
     *
     * @param name
     */
    public ChiefOfService(String name) {
        super(name);
    }

    /**
     * Chief of service to check the patient registration queue
     */
    public void checkPatientsInQueue() {
        System.out.println("[" + this + "] is going to assign doctor and nurses for new patients.");

        while (this.service.hasPatientsInRegistrationQueue()) {
            Patient patient = this.service.getPatientInRegistrationQueue();

            PatientRoom patientRoom = this.service.getHospital().getAvailablePatientRoom();

            if (null == patientRoom) {
                System.out.println("[" + this + "] The hospital is running out of patient rooms. So " + patient + " has been rejected.");
                return;
            }

            patientRoom.addPatient(patient);

            System.out.println("[" + this + "] Assigning doctor and nurses for " + patient);

            if (!service.getAvailableDoctor().isPresent()) {
                System.out.println("[" + this + "] There are too many patients in " + service + ". Asking " + service.getEmployeeHumanResource() + " to hire one more doctor.");
                service.getEmployeeHumanResource().hireDoctorForService(service);
            }
            Doctor doctor = service.getAvailableDoctor().get();

            if (!service.getAvailableNurse().isPresent()) {
                System.out.println("[" + this + "] There are too many patients in " + service + ". Asking " + service.getEmployeeHumanResource() + " to hire one more nurse.");
                service.getEmployeeHumanResource().hireNurseForService(service);
            }
            Nurse nurse = service.getAvailableNurse().get();

            service.acceptPatient(patient, doctor, nurse);

            System.out.println("[" + this + "] " + patient + " has been added to service " + service);
        }
    }

    @Override
    public String toString() {
        return "ChiefOfService{" +
                "name=" + name +
                '}';
    }
}
