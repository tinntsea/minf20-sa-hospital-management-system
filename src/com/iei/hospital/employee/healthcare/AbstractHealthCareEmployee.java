package com.iei.hospital.employee.healthcare;

import com.iei.hospital.employee.AbstractEmployee;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.service.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstract Health Care Employee entity
 */
abstract class AbstractHealthCareEmployee extends AbstractEmployee {

    /**
     * List of patients are being taken care of this employee
     */
    protected List<Patient> patients = new LinkedList<>();

    /**
     * Constructor
     * @param name
     */
    public AbstractHealthCareEmployee(String name) {
        super(name);
    }

    /**
     * Add patient to this employee
     * @param patient
     */
    public void addPatient(Patient patient) {
        this.patients.add(patient);

        System.out.println("["+this+"] is now taking care of " + patient);
    }

    /**
     * Get list of patients that are being taken care of this employee
     * @return
     */
    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * Remove patient from this employee
     * @param patient
     */
    public void removePatient(Patient patient) {
        this.patients.remove(patient);
    }

    /**
     * Count number of patients that are being taken care of this employee
     * @return
     */
    public int getNumberOfPatients() {
        return this.patients.size();
    }

    @Override
    public String toString() {
        return "HumanResource{" +
                "name='" + name + '\'' +
                '}';
    }
}
