package com.iei.hospital.employee.healthcare;

/**
 * Nurse entity
 */
public class Nurse extends AbstractHealthCareEmployee {

    /**
     * Constructor
     * @param name
     */
    public Nurse(String name) {
        super(name);
    }

    /**
     * Perform patient follow up
     */
    public void performPatientFollowUp() {

    }

    @Override
    public String toString() {
        return "Nurse{" +
                "name='" + name + '\'' +
                '}';
    }
}
