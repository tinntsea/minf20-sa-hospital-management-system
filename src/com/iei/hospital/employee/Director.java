package com.iei.hospital.employee;

import com.iei.hospital.Hospital;
import com.iei.hospital.building.Building;
import com.iei.hospital.building.Floor;
import com.iei.hospital.employee.humanresource.HumanResourceDirector;
import com.iei.hospital.room.Room;
import com.iei.hospital.room.RoomFactory;
import com.iei.hospital.room.RoomType;
import com.iei.hospital.service.Service;
import com.iei.hospital.service.ServiceActivityObserver;

/**
 * General director entity
 */
public class Director extends AbstractEmployee {

    /**
     * Director's hospital
     */
    private Hospital hospital;

    /**
     * Static instance of the director (Singleton)
     */
    private static Director instance;

    /**
     * Private Constructor to avoid creating new instance
     */
    private Director() {
        super("");
    }

    /**
     * Get director instance
     * @return Director
     */
    public static Director getInstance() {
        if (instance == null) {
            instance = new Director();
        }

        return instance;
    }

    /**
     * Get employee position
     * @return
     */
    @Override
    public String getPosition() {
        return "General Director";
    }

    /**
     * Add new service to the hospital
     * @param hospital
     * @param floor
     * @param name
     * @return Service
     */
    public Service addNewService(Hospital hospital, Floor floor, String name) {
        Service newService = new Service(hospital, floor, name);
        newService.addObserver(new ServiceActivityObserver());
        this.hospital.addService(newService);

        System.out.println(newService + " has been added to " + floor + " by " + this);

        hospital.getHumanResourceDirector().hireHumanResourceEmployeesForService(newService);

        return newService;
    }

    /**
     * Add new floor to the hospital
     * @param name
     * @return Floor
     */
    public Floor addNewFloor(Building building, String name) {
        Floor newFloor = new Floor(name);
        building.addFloor(newFloor);

        System.out.println("["+ this +"] has been added " + newFloor);

        return newFloor;
    }

    /**
     * Add room to floor
     * @param floor
     * @param roomType
     * @param name
     * @param capacity
     * @return
     */
    public Room addRoomToFloor(Floor floor, RoomType roomType, String name, int capacity) {
        Room newRoom = RoomFactory.createRoom(roomType, floor, name, capacity);
        floor.addRoom(newRoom);

        System.out.println("["+ this +"] added " + newRoom + " to " + floor);

        return newRoom;
    }

    /**
     * Hire human resource directoe
     * @param name
     */
    public void hireHumanResourceDirector(String name) {
        HumanResourceDirector hmDirector = new HumanResourceDirector(name);

        this.hospital.setHumanResourceDirector(hmDirector);
    }

    /**
     * Set the hospital
     * @param hospital
     */
    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    @Override
    public String toString() {
        return "Director{" +
                "name='" + name + '\'' +
                '}';
    }
}
