package com.iei.hospital.employee.staff;

/**
 * Inventory Staff
 */
public class InventoryStaff extends AbstractStaff {

    /**
     * Constructor
     * @param name
     */
    public InventoryStaff(String name) {
        super(name);
    }

    /**
     * Count used equipment
     */
    public void countUsedEquipments() {

    }

    @Override
    public String toString() {
        return "InventoryStaff{" +
                "name='" + name + '\'' +
                '}';
    }

}
