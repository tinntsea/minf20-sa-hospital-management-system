package com.iei.hospital.employee.staff;

import com.iei.hospital.equipment.Equipment;
import com.iei.hospital.equipment.EquipmentType;

/**
 * Laundry Staff
 */
public class LaundryStaff extends AbstractStaff {

    /**
     * Constructor
     * @param name
     */
    public LaundryStaff(String name) {
        super(name);
    }

    public void startLaundry() {
        Equipment equipment = this.service.getHospital().getEquipments(EquipmentType.STAFF_EQUIPMENT, 1).get(0);

        System.out.println("["+ this +"] has started laundry using " + equipment);
    }

    @Override
    public String toString() {
        return "LaundryStaff{" +
                "name='" + name + '\'' +
                '}';
    }
}
