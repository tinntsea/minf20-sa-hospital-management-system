package com.iei.hospital.employee.staff;

import com.iei.hospital.employee.AbstractEmployee;

/**
 * Abstract Staff
 */
public class AbstractStaff extends AbstractEmployee  {
    /**
     * Constructor
     * @param name
     */
    public AbstractStaff(String name) {
        super(name);
    }

}
