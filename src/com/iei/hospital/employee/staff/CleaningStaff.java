package com.iei.hospital.employee.staff;

import com.iei.hospital.equipment.Equipment;
import com.iei.hospital.equipment.EquipmentType;

/**
 * Cleaning Staff
 */
public class CleaningStaff extends AbstractStaff {

    /**
     * Constructor
     * @param name
     */
    public CleaningStaff(String name) {
        super(name);
    }

    /**
     * Do cleaning work
     */
    public void startCleaning() {
        Equipment equipment = this.service.getHospital().getEquipments(EquipmentType.STAFF_EQUIPMENT, 1).get(0);

        System.out.println("["+ this +"] has started cleaning using " + equipment);
    }

    @Override
    public String toString() {
        return "CleaningStaff{" +
                "name='" + name + '\'' +
                '}';
    }
}
