package com.iei.hospital.employee.humanresource;

import com.iei.hospital.employee.AbstractEmployee;
import com.iei.hospital.employee.healthcare.Doctor;
import com.iei.hospital.employee.healthcare.Nurse;
import com.iei.hospital.finance.Invoice;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.service.Service;
import com.iei.util.EmployeeNameGenerator;

/**
 * Human resource entity
 */
public class HumanResourceEmployee extends AbstractEmployee {

    /**
     * Constructor
     * @param name
     */
    public HumanResourceEmployee(String name) {
        super(name);
    }

    /**
     * Hire a doctor for a service
     * @param service
     */
    public void hireDoctorForService(Service service) {
        Doctor newDoctor = new Doctor(EmployeeNameGenerator.generateName("Doctor"));
        service.addDoctor(newDoctor);

        System.out.println("[" + this + "] has hire " + newDoctor + " for " + service);
    }

    /**
     * Hire a nurse for a service
     * @param service
     */
    public void hireNurseForService(Service service) {
        Nurse newNurse = new Nurse(EmployeeNameGenerator.generateName("Nurse"));
        service.addNurse(newNurse);

        System.out.println("[" + this + "] has hire " + newNurse + " for " + service);
    }

    /**
     * Acknowledge patient arrival
     * @param service
     * @param patient
     */
    public void acknowledgePatientArrival(Service service, Patient patient) {
        System.out.println("[" + this + "] acknowledged that " + patient + " has arrived to " + service);
    }

    /**
     * Create invoice
     * @param patient
     * @param invoiceTemplate
     * @return
     */
    public String createInvoice(Patient patient, Invoice invoiceTemplate) {
        return invoiceTemplate.getInvoiceDetails(patient);
    }

    @Override
    public String toString() {
        return "HumanResourceEmployee{" +
                "name='" + name + '\'' +
                '}';
    }
}
