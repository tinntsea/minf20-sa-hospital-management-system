package com.iei.hospital.employee.humanresource;

import com.iei.hospital.service.Service;
import com.iei.util.EmployeeNameGenerator;

/**
 * Human resource director entity
 */
public class HumanResourceDirector extends HumanResourceEmployee {

    /**
     * Constructor
     * @param name
     */
    public HumanResourceDirector(String name) {
        super(name);
    }

    public void hireHumanResourceEmployeesForService(Service service) {
        HumanResourceEmployee employeeHSE = new HumanResourceEmployee(EmployeeNameGenerator.generateName("Employee Human Resource"));
        service.setEmployeeHumanResource(employeeHSE);

        HumanResourceEmployee patientHSE = new HumanResourceEmployee(EmployeeNameGenerator.generateName("Patient Human Resource"));
        service.setPatientHumanResource(patientHSE);

        System.out.println(this + " has hired " + employeeHSE + " and " + patientHSE + " for " + service);
    }

    @Override
    public String toString() {
        return "HumanResourceDirector{" +
                "name='" + name + '\'' +
                '}';
    }
}
