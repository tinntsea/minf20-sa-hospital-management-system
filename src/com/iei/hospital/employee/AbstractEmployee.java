package com.iei.hospital.employee;

import com.iei.hospital.service.Service;

/**
 * Abstract employee
 */
public abstract class AbstractEmployee implements Employee {

    /**
     * Employee name
     */
    protected String name;

    /**
     * Employee position
     */
    protected String position = "";

    /**
     * Employee's license
     */
    protected String license = "";

    /**
     * Service
     */
    protected Service service;

    /**
     * Constructor
     * @param name
     */
    public AbstractEmployee(String name) {
        this.name = name;
    }

    /**
     * Get employee name
     * @return
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Set employee name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get license
     * @return String
     */
    @Override
    public String getLicense() {
        return this.license;
    }

    /**
     * Get employee position
     * @return String
     */
    @Override
    public String getPosition() {
        return this.position;
    }

    /**
     * Set service
     * @param service
     */
    public void setService(Service service) {
        this.service = service;
    }

}
