package com.iei.hospital.disease;

/**
 * Disease entity
 */
public class Disease {

    /**
     * Name of the disease
     */
    private final String name;

    /**
     * Disease type
     */
    private DiseaseType type;

    /**
     * Constructor
     * @param name
     * @param type
     */
    public Disease(String name, DiseaseType type) {
        this.name = name;
        this.type = type;
    }

    /**
     * Get name of the disease
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Get disease type
     * @return String
     */
    public DiseaseType getType() {
        return type;
    }

    /**
     * Set disease type
     * @param type
     */
    public void setType(DiseaseType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Disease{" +
                "name='" + name + '\'' +
                '}';
    }

}
