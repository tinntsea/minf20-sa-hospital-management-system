package com.iei.hospital.disease;

/**
 * Disease type enum
 */
public enum DiseaseType {
    MEDICAL_TYPE, OPERATION_TYPE
}
