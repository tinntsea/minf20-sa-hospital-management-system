package com.iei.hospital;

import com.iei.hospital.building.Building;
import com.iei.hospital.building.Floor;
import com.iei.hospital.disease.DiseaseType;
import com.iei.hospital.employee.Director;
import com.iei.hospital.employee.healthcare.Doctor;
import com.iei.hospital.employee.humanresource.HumanResourceDirector;
import com.iei.hospital.equipment.Equipment;
import com.iei.hospital.equipment.EquipmentType;
import com.iei.hospital.finance.TextualInvoice;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.patient.PatientStatus;
import com.iei.hospital.patient.TreatmentActivityRecord;
import com.iei.hospital.room.*;
import com.iei.hospital.service.Service;
import com.iei.util.NumberUtil;

import java.util.*;

/**
 * Hospital entity
 */
public class Hospital {

    /**
     * Hospital name
     */
    private final String name;

    /**
     * Building
     */
    private final Building building;

    /**
     * Services in the hospital
     */
    private final Set<Service> services = new HashSet<>();

    /**
     * Equipments of the hospital
     */
    private final Set<Equipment> equipments = new HashSet<>();

    /**
     * General director of the hospital
     */
    private Director generalDirector;

    /**
     * Human resource director of the hospital
     */
    private HumanResourceDirector humanResourceDirector;

    /**
     * Constructor
     *
     * @param name
     * @param building
     */
    public Hospital(String name, Building building) {
        this.name = name;
        this.building = building;

        System.out.println(this + " has been created");
    }

    /**
     * Set director for the hospital
     * @param director
     */
    public void setDirector(Director director) {
        this.generalDirector = director;

        this.generalDirector.setHospital(this);

        System.out.println(director + " is the general director of " + this);
    }

    /**
     * Set human resource for the director
     * @param humanResourceDirector
     */
    public void setHumanResourceDirector(HumanResourceDirector humanResourceDirector) {
        this.humanResourceDirector = humanResourceDirector;

        System.out.println(humanResourceDirector + " is the human resource director of " + this);
    }

    /**
     * Get name of the hospital
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get general director of the hospital
     * @return
     */
    public Director getGeneralDirector() {
        return generalDirector;
    }

    /**
     * Get building
     * @return
     */
    public Building getBuilding() {
        return building;
    }

    /**
     * Add new service to the hospital
     * @param service
     */
    public void addService(Service service) {
        this.services.add(service);
    }

    /**
     * Get list of services in the hospital
     * @return
     */
    public Set<Service> getServices() {
        return services;
    }

    /**
     * Add a new equipment to the hospital
     * @param equipment
     */
    public void addEquipment(Equipment equipment) {
        this.equipments.add(equipment);
    }

    /**
     * Get all equipments in the hospital
     * @return
     */
    public Set<Equipment> getEquipments() {
        return equipments;
    }

    /**
     * Get human resource director
     * @return
     */
    public HumanResourceDirector getHumanResourceDirector() {
        return humanResourceDirector;
    }

    /**
     * Get available patient room
     * @return
     */
    public PatientRoom getAvailablePatientRoom() {
        for (Floor floor : this.getBuilding().getFloors()) {
            for (Room room : floor.getRooms()) {
                if (room instanceof PatientRoom && room.isAvailable()) {
                    return (PatientRoom) room;
                }
            }
        }

        return null;
    }

    /**
     * Get operation room
     * @return
     */
    public OperationRoom getOperationRoom() {
        for (Floor floor : this.getBuilding().getFloors()) {
            for (Room room : floor.getRooms()) {
                if (room instanceof OperationRoom && room.isAvailable()) {
                    return (OperationRoom) room;
                }
            }
        }

        return null;
    }

    /**
     * Get equipments for use
     * @param type
     * @param quantity
     * @return
     */
    public List<Equipment> getEquipments(EquipmentType type, int quantity) {
        List<Equipment> equipments = new LinkedList<>();

        for (Equipment e : this.getEquipments()) {
            if (e.getType() == type) {
                equipments.add(e);
                this.getEquipments().remove(e);
            }

            if (equipments.size() == quantity) {
                break;
            }
        }

        return equipments;
    }

    public void orderEquipments(EquipmentType type, int quantity) {
        for (int i = 0; i < quantity; i++) {
            this.addEquipment(new Equipment(type, type.toString()));
        }

        System.out.println("There are " + quantity + " equipments " + type.toString() + " have been added");
    }

    public void checkPatients() {
        for (Service service : this.getServices()) {
            for (Doctor doctor : service.getDoctors()) {
                doctor.checkPatients();
            }
        }
    }

    /**
     * Checkout healed patients
     */
    public void checkOutPatients() {
        for (Service service : this.getServices()) {
            ArrayList<Patient> patients =  new ArrayList<>(service.getPatientsHealed());
            for (Patient patient : patients) {
                String invoice = service.getPatientHumanResource().createInvoice(patient, new TextualInvoice());
                System.out.println(invoice);
                service.checkoutPatient(patient);
            }
        }
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "name='" + name + '\'' +
                '}';
    }
}
