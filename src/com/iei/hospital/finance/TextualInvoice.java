package com.iei.hospital.finance;

import com.iei.hospital.disease.DiseaseType;
import com.iei.hospital.patient.Patient;
import com.iei.hospital.patient.TreatmentActivityRecord;

/**
 * Textual Invoice
 */
public class TextualInvoice implements Invoice {

    /**
     * Get invoice details
     * @param patient
     * @return String
     */
    @Override
    public String getInvoiceDetails(Patient patient) {
        double totalAmount = 0.0;

        String result = "Invoice for " + patient;

        int roomFee = (patient.getPatientRoom().getCapacity()) > 1 ? 30 : 50;
        totalAmount += roomFee;
        result += "\n\tRoom fee: " + roomFee;

        int count = 0;
        for (TreatmentActivityRecord record : patient.getTreatmentActivityRecords()) {
            int treatmentFee =  (record.getDiseaseType() == DiseaseType.MEDICAL_TYPE) ? 80 : 100;
            result += "\n\tTreatment fee " + ++count + ": " + treatmentFee;

            totalAmount += treatmentFee;
        }

        result += "\n\tTotal amount:  " + totalAmount;

        return result;
    }

}
