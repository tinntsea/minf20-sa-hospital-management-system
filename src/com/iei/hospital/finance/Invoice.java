package com.iei.hospital.finance;

import com.iei.hospital.patient.Patient;

/**
 * Create Invoice
 */
public interface Invoice {
    String getInvoiceDetails(Patient patient);
}
