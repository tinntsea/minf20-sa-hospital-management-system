package com.iei.hospital.room;

import com.iei.hospital.building.Floor;
import com.iei.hospital.employee.Employee;
import java.util.HashSet;
import java.util.Set;

/**
 * Office Room entity
 */
public class OfficeRoom extends AbstractRoom implements OfficeRoomInterface {

    /**
     * List of employees in this room
     */
    private final Set<Employee> employees = new HashSet<>();

    /**
     * Constructor
     * @param floor
     * @param name
     * @param capacity
     */
    public OfficeRoom(Floor floor, String name, int capacity) {
        super(floor, RoomType.OFFICE_ROOM, name, capacity);
    }

    /**
     * Add an employee to the room
     * @param employee
     */
    @Override
    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }

    /**
     * Check if this room is available for use
     * @return
     */
    @Override
    public boolean isAvailable() {
        return this.employees.size() < this.capacity;
    }

    @Override
    public String toString() {
        return "OfficeRoom{" +
                "name='" + name + '\'' +
                '}';
    }

}
