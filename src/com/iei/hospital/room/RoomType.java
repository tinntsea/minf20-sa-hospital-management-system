package com.iei.hospital.room;

/**
 * Room Type Enum
 */
public enum RoomType {
    PATIENT_ROOM, OFFICE_ROOM, STORAGE_ROOM, OPERATION_ROOM
}
