package com.iei.hospital.room;

import com.iei.hospital.building.Floor;
import com.iei.hospital.employee.Director;
import com.iei.hospital.employee.Employee;

import java.util.LinkedList;
import java.util.List;

/**
 * An office room proxy class which helps to enforce that only director could be assigned to director room
 */
public class DirectorRoomProxy implements OfficeRoomInterface {

    /**
     * The real room class
     */
    private final OfficeRoomInterface room;

    /**
     * Constructor
     * @param room
     */
    public DirectorRoomProxy(OfficeRoomInterface room) {
        this.room = room;
    }

    /**
     * Get name
     * @return
     */
    @Override
    public String getName() {
        return this.room.getName();
    }

    /**
     * Get floor
     * @return
     */
    @Override
    public Floor getFloor() {
        return this.room.getFloor();
    }

    /**
     * Get capacity
     * @return
     */
    @Override
    public int getCapacity() {
        return this.room.getCapacity();
    }

    /**
     * Get type
     * @return
     */
    @Override
    public RoomType getType() {
        return this.room.getType();
    }

    /**
     * Add employee
     * @param employee
     * @throws IllegalArgumentException
     */
    @Override
    public void addEmployee(Employee employee) throws IllegalArgumentException {
        if (employee instanceof Director) {
            this.room.addEmployee(employee);

            System.out.println(employee + " now sitting in " + room);
            return;
        }

        throw new IllegalArgumentException("Only director could be added to this room");
    }

    /**
     * Check if room is available for use
     * @return
     */
    @Override
    public boolean isAvailable() {
        return this.room.isAvailable();
    }

}
