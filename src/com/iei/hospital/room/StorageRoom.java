package com.iei.hospital.room;

import com.iei.hospital.building.Floor;
import com.iei.hospital.service.Service;

/**
 * Storage room
 */
public class StorageRoom extends AbstractRoom {

    /**
     * The service that this room belongs to
     */
    private Service service;

    /**
     * Constructor
     * @param floor
     * @param name
     * @param capacity
     */
    public StorageRoom(Floor floor, String name, int capacity) {
        super(floor, RoomType.STORAGE_ROOM, name, capacity);
    }

    /**
     * Set service
     * @param service
     */
    public void setService(Service service) {
        this.service = service;
    }

    /**
     * Get service
     * @return
     */
    public Service getService() {
        return service;
    }

    /**
     * Check if the room is available for use
     * @return
     */
    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public String toString() {
        return "StorageRoom{" +
                "name='" + name + '\'' +
                '}';
    }
}
