package com.iei.hospital.room;


import com.iei.hospital.building.Floor;

/**
 * Abstract room
 */
abstract class AbstractRoom implements Room {

    /**
     * Room name
     */
    protected final String name;

    /**
     * Room type
     */
    protected final RoomType type;

    /**
     * Floor where room belongs to
     */
    protected final Floor floor;

    /**
     * Room capacity
     */
    protected final int capacity;

    /**
     * Constructor
     * @param floor
     * @param type
     * @param name
     * @param capacity
     */
    public AbstractRoom(Floor floor, RoomType type, String name, int capacity) {
        this.name = name;
        this.type = type;
        this.floor = floor;
        this.capacity = capacity;
    }

    /**
     * Get room name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Get floor
     * @return
     */
    public Floor getFloor() {
        return floor;
    }

    /**
     * Get capacity
     * @return
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Get type
     * @return
     */
    public RoomType getType() {
        return type;
    }

    /**
     * Check if room is available for use
     * @return boolean
     */
    public abstract boolean isAvailable();
}
