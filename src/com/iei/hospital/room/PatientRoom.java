package com.iei.hospital.room;

import com.iei.hospital.building.Floor;
import com.iei.hospital.patient.Patient;
import java.util.LinkedList;
import java.util.List;

/**
 * Patient room
 */
public class PatientRoom extends AbstractRoom {

    /**
     * List of patients in the room
     */
    private final List<Patient> patients = new LinkedList<>();

    /**
     * Constructor
     * @param floor
     * @param name
     * @param capacity
     */
    public PatientRoom(Floor floor, String name, int capacity) {
        super(floor, RoomType.PATIENT_ROOM, name, capacity);
    }

    /**
     * Add patient into the room
     * @param patient
     */
    public void addPatient(Patient patient) {
        this.patients.add(patient);
        patient.setPatientRoom(this);

        System.out.println(patient + " has been assigned to " + this);
    }

    /**
     * Remove a patient from the room
     * @param patient
     */
    public void removePatient(Patient patient) {
        this.patients.remove(patient);

        System.out.println(patient + " has been removed from " + this);
    }

    /**
     * Check if the room is available for use
     * @return
     */
    @Override
    public boolean isAvailable() {
        return this.patients.size() < this.capacity;
    }

    @Override
    public String toString() {
        return "PatientRoom{" +
                "name='" + name + '\'' +
                '}';
    }

}
