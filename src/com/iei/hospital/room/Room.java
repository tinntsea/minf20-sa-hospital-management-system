package com.iei.hospital.room;

import com.iei.hospital.building.Floor;

/**
 * Room Interface
 */
public interface Room {

    /**
     * Get room type
     * @return
     */
    RoomType getType();

    /**
     * Get room name
     * @return
     */
    String getName();

    /**
     * Get floor which the room belongs to
     * @return
     */
    Floor getFloor();

    /**
     * Get capacity of the room
     * @return
     */
    int getCapacity();

    /**
     * Check if the room is available for use
     * @return
     */
    boolean isAvailable();
}
