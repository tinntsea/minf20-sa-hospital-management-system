package com.iei.hospital.room;


import com.iei.hospital.building.Floor;

/**
 * Room Factory
 */
public class RoomFactory {

    /**
     * To create a new room
     * @param type
     * @param floor
     * @param name
     * @param capacity
     * @return
     * @throws IllegalArgumentException
     */
    public static Room createRoom(RoomType type, Floor floor, String name, int capacity)
            throws IllegalArgumentException {
        switch (type) {
            case PATIENT_ROOM:
                return new PatientRoom(floor, name, capacity);
            case OFFICE_ROOM:
                return new OfficeRoom(floor, name, capacity);
            case STORAGE_ROOM:
                return new StorageRoom(floor, name, capacity);
            case OPERATION_ROOM:
                return new OperationRoom(floor, name, capacity);
        }

        throw new IllegalArgumentException("Invalid room type");
    }

}
