package com.iei.hospital.room;

import com.iei.hospital.employee.Employee;

/**
 * Room Interface
 */
public interface OfficeRoomInterface extends Room {
    void addEmployee(Employee employee);
}
