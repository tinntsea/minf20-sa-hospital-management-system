package com.iei.hospital.room;


import com.iei.hospital.building.Floor;

/**
 * Operation Room entity
 */
public class OperationRoom extends AbstractRoom {

    /**
     * If the room is currently in use
     */
    private boolean isInUse = false;

    /**
     * Constructor
     * @param floor
     * @param name
     * @param capacity
     */
    public OperationRoom(Floor floor, String name, int capacity) {
        super(floor, RoomType.OPERATION_ROOM, name, capacity);
    }

    /**
     * Set the room is in use or use
     * @param inUse
     */
    public void setInUse(Boolean inUse) {
        this.isInUse = inUse;
    }

    /**
     * Check if the room is available for use
     * @return
     */
    @Override
    public boolean isAvailable() {
        return !this.isInUse;
    }

    @Override
    public String toString() {
        return "OperationRoom{" +
                "name='" + name + '\'' +
                '}';
    }
}
