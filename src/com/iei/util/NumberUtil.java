package com.iei.util;

import java.util.Random;

public class NumberUtil {

    /**
     * Returns a random number from min to max
     * @param min int
     * @param max int
     * @return Int
     */
    public static int getRandomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

}
