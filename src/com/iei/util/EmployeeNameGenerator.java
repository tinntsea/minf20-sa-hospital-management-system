package com.iei.util;

public class EmployeeNameGenerator {
    private static int counter = 0;

    /**
     * Generate an employee name
     * @param prefix
     * @return name
     */
    public static String generateName(String prefix) {
        return prefix + " " + ++counter;
    }
}
