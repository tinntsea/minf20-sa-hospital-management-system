

# Hospital Management System

## Instructors
- CM - Professor Sansen Joris
- TD - Ngo Ngoc Dang Khoa
  
## Students:
- Nguyen Trong Tin
- Tran Dinh Nam Duong
- Huynh Huu Nghia
- Le Dinh Than

## Source code
The source code of the project is at [https://gitlab.com/tinntsea/minf20-sa-hospital-management-system](https://gitlab.com/tinntsea/minf20-sa-hospital-management-system)

## Report
The project report is located in `docs/report` directory. The report is in docx and pdf formats.

## Working Compiled Java Program
The working compiled java program is located in `jar/HMS.jar`

To run the pre-compiled program, please run:
```
java -jar jar/HMS.jar
```

## Code documentation 
The code documentation is located in `docs/javadoc` directory in html format.
Please open the `docs/javadoc/index.html` to open the doc.




